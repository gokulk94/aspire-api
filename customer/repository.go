package customer

import (
	"context"
	"database/sql"
	"encoding/json"
	"time"

	"gitlab.com/gokulk94/aspire-api/utils"
)

type iCustomerRepo interface {
	StartTransaction(ctx context.Context) (*sql.Tx, error)
	Commit(ctx context.Context, tx *sql.Tx) error
	Rollback(ctx context.Context, tx *sql.Tx) error
	InsertNewLoanRequest(ctx context.Context, tx *sql.Tx, req PostNewLoanRequest) (uint64, error)
	InsertScheduledPayment(ctx context.Context, tx *sql.Tx, IDLoanRequest, IDCustomer, amount uint64, paymentDate time.Time) (uint64, error)
	GetLoanList(ctx context.Context, idCustomer uint64) ([]Loan, error)
	GetLoanDetails(ctx context.Context, customerID, loanID uint64) (Loan, error)
	GetNextPendingPaymentSchedule(ctx context.Context, loanID uint64) (ScheduledPayment, error)
	ChangePaymentStatus(ctx context.Context, tx *sql.Tx, schedulePaymentID, amountPaid uint64, newStatus string) error
	GetPaidDetails(ctx context.Context, tx *sql.Tx, loadID uint64) (uint64, uint64, error)
	DeletePendingPayments(ctx context.Context, tx *sql.Tx, loadID uint64) error
	IsLoanPaymentComplete(ctx context.Context, tx *sql.Tx, loadID uint64) (bool, error)
	ChangeLoanStatus(ctx context.Context, tx *sql.Tx, loadID uint64, newStatus string) error
}

type CustomerRepo struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) iCustomerRepo {
	return &CustomerRepo{db}
}

func (r *CustomerRepo) StartTransaction(ctx context.Context) (*sql.Tx, error) {
	return r.db.Begin()
}

func (r *CustomerRepo) Commit(ctx context.Context, tx *sql.Tx) error {
	return tx.Commit()
}

func (r *CustomerRepo) Rollback(ctx context.Context, tx *sql.Tx) error {
	return tx.Rollback()
}

func (r *CustomerRepo) InsertNewLoanRequest(ctx context.Context, tx *sql.Tx, req PostNewLoanRequest) (uint64, error) {
	var id uint64
	err := tx.QueryRowContext(ctx, `
	INSERT INTO public.loan_request (id_customer, loan_amount, loan_term, status, created_at, updated_at)
		VALUES ($1, $2, $3, $4, now(), now())
	RETURNING
		id_loan_request
	`, req.CustomerID, req.LoanAmount, req.LoanTerm, statusPending).Scan(&id)
	return id, err
}

func (r *CustomerRepo) InsertScheduledPayment(ctx context.Context, tx *sql.Tx, IDLoanRequest, IDCustomer, amount uint64, paymentDate time.Time) (uint64, error) {
	var paymentID uint64
	err := tx.QueryRowContext(ctx, `
	INSERT INTO public.scheduled_payment (id_customer, id_loan_request, amount_to_pay, payment_date, status, created_at, updated_at)
  	VALUES ($1, $2, $3, $4, $5, now(), now())
	RETURNING
		id_scheduled_payment
	`, IDCustomer, IDLoanRequest, amount, paymentDate, statusPending).Scan(&paymentID)
	return paymentID, err
}

func (r *CustomerRepo) GetLoanList(ctx context.Context, idCustomer uint64) ([]Loan, error) {
	loans := []Loan{}
	query := `
	SELECT
		lr.id_loan_request,
		lr.loan_amount,
		lr.loan_term,
		lr.status,
		json_agg(json_build_object('id_scheduled_payment', sp.id_scheduled_payment, 'amount_to_pay', sp.amount_to_pay, 'payment_date', sp.payment_date, 'status', sp.status)
		ORDER BY sp.payment_date ASC)
	FROM
		loan_request lr
		JOIN scheduled_payment sp ON sp.id_loan_request = lr.id_loan_request
	WHERE
		lr.id_customer = $1
	GROUP BY
		lr.id_loan_request
	ORDER BY
		lr.created_at ASC
	`
	rows, err := r.db.QueryContext(ctx, query, idCustomer)
	if err != nil {
		return loans, err
	}
	defer rows.Close()

	for rows.Next() {
		var l Loan
		var pdStr string
		err := rows.Scan(&l.LoadRequestID, &l.LoanAmount, &l.LoanTerm, &l.Status, &pdStr)
		if err != nil {
			return loans, err
		}
		var sp []PGScheduledPayment
		err = json.Unmarshal([]byte(pdStr), &sp)
		if err != nil {
			return loans, err
		}
		for _, s := range sp {
			var a ScheduledPayment

			a.ScheduledPaymentID = s.ScheduledPaymentID
			a.AmountToPay = s.AmountToPay
			a.Status = s.Status
			t, err := utils.ParseTime(s.PaymentDate)
			if err != nil {
				return loans, err
			}
			a.PaymentDate = t
			l.ScheduledPayments = append(l.ScheduledPayments, a)
		}
		loans = append(loans, l)
	}
	return loans, nil
}

func (r *CustomerRepo) GetLoanDetails(ctx context.Context, customerID, loanID uint64) (Loan, error) {
	var result Loan
	query := `
	SELECT
		lr.id_loan_request,
		lr.loan_amount,
		lr.loan_term,
		lr.status
	FROM
		loan_request lr
	WHERE
		lr.id_customer = $1
		AND lr.id_loan_request = $2
	`
	err := r.db.QueryRowContext(ctx, query, customerID, loanID).Scan(&result.LoadRequestID, &result.LoanAmount, &result.LoanTerm, &result.Status)
	if err != nil {
		return result, nil
	}
	return result, nil
}

func (r *CustomerRepo) GetNextPendingPaymentSchedule(ctx context.Context, loanID uint64) (ScheduledPayment, error) {
	var sp ScheduledPayment
	query := `
	SELECT
		id_scheduled_payment,
		amount_to_pay,
		payment_date,
		status
	FROM
		scheduled_payment sp
	WHERE
		id_loan_request = $1
		AND status = $2
	ORDER BY
		payment_date ASC
	LIMIT 1
	`
	err := r.db.QueryRowContext(ctx, query, loanID, statusPending).Scan(&sp.ScheduledPaymentID, &sp.AmountToPay, &sp.PaymentDate, &sp.Status)
	if err != nil {
		return sp, err
	}
	return sp, nil
}

func (r *CustomerRepo) ChangePaymentStatus(ctx context.Context, tx *sql.Tx, schedulePaymentID, amountPaid uint64, newStatus string) error {
	_, err := tx.ExecContext(ctx, `
	UPDATE
		scheduled_payment
	SET
		status = $1,
		amount_paid = $2,
		updated_at = now()
	WHERE
		id_scheduled_payment = $3
	`, newStatus, amountPaid, schedulePaymentID)
	return err
}

func (c *CustomerRepo) GetPaidDetails(ctx context.Context, tx *sql.Tx, loadID uint64) (uint64, uint64, error) {
	var (
		totalPaid uint64
		count     uint64
	)
	query := `
	SELECT
		COALESCE(sum(amount_paid), 0),
		count(*)
	FROM
		scheduled_payment sp
	WHERE
		id_loan_request = $1
		AND status = $2
	`
	err := tx.QueryRowContext(ctx, query, loadID, statusPaid).Scan(&totalPaid, &count)
	return totalPaid, count, err
}

func (c *CustomerRepo) DeletePendingPayments(ctx context.Context, tx *sql.Tx, loadID uint64) error {
	query := `
	DELETE FROM
		scheduled_payment sp
	WHERE
		id_loan_request = $1
		AND status = $2
	`
	_, err := tx.ExecContext(ctx, query, loadID, statusPending)
	return err
}

func (c *CustomerRepo) IsLoanPaymentComplete(ctx context.Context, tx *sql.Tx, loadID uint64) (bool, error) {
	var doesPendingPaymentExist bool
	query := `
	SELECT EXISTS (
		SELECT
			id_scheduled_payment
		FROM
			scheduled_payment
		WHERE
			id_loan_request = $1
			AND status = $2
	)
	`
	err := tx.QueryRowContext(ctx, query, loadID, statusPending).Scan(&doesPendingPaymentExist)
	return !doesPendingPaymentExist, err
}

func (r *CustomerRepo) ChangeLoanStatus(ctx context.Context, tx *sql.Tx, loadID uint64, newStatus string) error {
	_, err := tx.ExecContext(ctx, `
	UPDATE
		loan_request
	SET
		status = $1,
		updated_at = now()
	WHERE
		id_loan_request = $2
	`, newStatus, loadID)
	return err
}
