package customer

import "time"

type PostNewLoanResponse struct {
	LoadRequestID    uint64             `json:"id_load_request"`
	ScheduledPayment []ScheduledPayment `json:"scheduled_payment"`
}

type GetLoanListResponse struct {
	CustomerID uint64 `json:"customer_id"`
	Loans      []Loan `json:"loans"`
}

type Loan struct {
	LoadRequestID     uint64             `json:"id_loan_request"`
	LoanAmount        uint64             `json:"loan_amount"`
	LoanTerm          uint64             `json:"loan_term"`
	Status            string             `json:"status"`
	ScheduledPayments []ScheduledPayment `json:"scheduled_payment"`
}

type PGScheduledPayment struct {
	ScheduledPaymentID uint64 `json:"id_scheduled_payment"`
	AmountToPay        uint64 `json:"amount_to_pay"`
	PaymentDate        string `json:"payment_date"`
	Status             string `json:"status"`
}
type ScheduledPayment struct {
	ScheduledPaymentID uint64    `json:"id_scheduled_payment"`
	AmountToPay        uint64    `json:"amount_to_pay"`
	PaymentDate        time.Time `json:"payment_date"`
	Status             string    `json:"status"`
}

type RegisterPaymentResponse struct {
	ScheduledPaymentID uint64             `json:"id_scheduled_payment"`
	PaymentDate        time.Time          `json:"payment_date"`
	RemainingPayments  []ScheduledPayment `json:"remaining_payments,omitempty"`
}
