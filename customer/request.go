package customer

type PostNewLoanRequest struct {
	CustomerID uint64 `json:"-"`
	LoanAmount uint64 `json:"loan_amount"` // Accept only smallest unit of currency. Cent.
	LoanTerm   uint64 `json:"loan_term"`
}

type GetLoanListRequest struct {
	CustomerID uint64 `json:"-"`
}

type RegisterPaymentRequest struct {
	CustomerID uint64 `json:"-"`
	LoanID     uint64 `json:"loan_id"`
	AmountPaid uint64 `json:"amount_paid"`
}
