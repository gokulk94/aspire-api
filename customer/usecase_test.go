package customer

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"
)

type usecaseMocks struct {
	repository mockICustomerRepo
}

func newUsecase(t *testing.T) (*CustomerUsecase, *usecaseMocks) {
	mocks := &usecaseMocks{}

	return &CustomerUsecase{
		&mocks.repository,
	}, mocks
}

func TestCustomerUsecase_PostNewLoan(t *testing.T) {

	request := PostNewLoanRequest{
		CustomerID: 1,
		LoanAmount: 10000,
		LoanTerm:   5,
	}
	tests := map[string]struct {
		request  PostNewLoanRequest
		setMocks func(*usecaseMocks)
		wantErr  bool
	}{
		"success case: new loan request was made successfully": {
			request: request,
			setMocks: func(um *usecaseMocks) {
				um.repository.Mock.On("StartTransaction", mock.Anything).Return(nil, nil)
				um.repository.Mock.On("Rollback", mock.Anything, mock.Anything).Return(nil)
				um.repository.Mock.On("Commit", mock.Anything, mock.Anything).Return(nil)
				um.repository.Mock.On("InsertNewLoanRequest", mock.Anything, mock.Anything, request).Return(uint64(1), nil)
				um.repository.Mock.On("InsertScheduledPayment", mock.Anything, mock.Anything, uint64(1), uint64(1), uint64(2000), mock.Anything).Return(uint64(1), nil)
			},
			wantErr: false,
		},
	}
	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			u, um := newUsecase(t)
			tc.setMocks(um)
			_, err := u.PostNewLoan(context.Background(), tc.request)
			if (err != nil) != tc.wantErr {
				t.Errorf("CustomerUsecase.PostNewLoan() error = %v, wantErr %v", err, tc.wantErr)
				return
			}
		})
	}
}
