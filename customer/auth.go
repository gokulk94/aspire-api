// A dummy auth middleware to simulate authenticated user.
// For demonstration purpose only.

package customer

import (
	"fmt"

	"github.com/go-chi/jwtauth/v5"
)

var tokenAuth *jwtauth.JWTAuth

const secret = "some secret for consumer"

func init() {
	tokenAuth = jwtauth.New("HS256", []byte(secret), nil)

	// For debugging/example purposes, we generate and print
	// a sample jwt token with claims `user_id:123` here:
	_, tokenString, _ := tokenAuth.Encode(map[string]interface{}{"customer_id": 1})
	fmt.Printf("DEBUG: a sample customer jwt is %s\n\n", tokenString)
}
