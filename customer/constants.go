package customer

import (
	"fmt"
	"time"
)

const (
	statusPending  = "PENDING"
	statusApproved = "APPROVED"
	statusRejected = "REJECTED"
	statusPaid     = "PAID"
)

var (
	weekDuration = 7 * 24 * time.Hour
)

var (
	ErrLoanNotApproved = fmt.Errorf("Loan not approved yet or already paid, so cannot make payment")
	ErrInvalidPayment  = fmt.Errorf("Cannot accept payment, as incoming amount less than scheduled payment amount")
	ErrExcessAmount    = fmt.Errorf("Cannot pay more than total loan amount. Please try with the correct amount")
)
