package customer

import (
	"context"
	"fmt"
	"log"
	"time"
)

// iCustomerUsecase interface
type iCustomerUsecase interface {
	PostNewLoan(context.Context, PostNewLoanRequest) (PostNewLoanResponse, error)
	GetLoanList(ctx context.Context, req GetLoanListRequest) (GetLoanListResponse, error)
	RegisterPayment(ctx context.Context, req RegisterPaymentRequest) (RegisterPaymentResponse, error)
}

type CustomerUsecase struct {
	repository iCustomerRepo
}

func NewUsecase(r iCustomerRepo) iCustomerUsecase {
	return &CustomerUsecase{r}
}

func (u *CustomerUsecase) PostNewLoan(ctx context.Context, req PostNewLoanRequest) (PostNewLoanResponse, error) {
	var resp PostNewLoanResponse

	tx, err := u.repository.StartTransaction(ctx)
	if err != nil {
		return resp, err
	}
	defer func() {
		if err != nil {
			err := u.repository.Rollback(ctx, tx)
			if err != nil {
				log.Println(err)
			}
			return
		}
		err := u.repository.Commit(ctx, tx)
		if err != nil {
			log.Println(err)
		}
	}()

	loanID, err := u.repository.InsertNewLoanRequest(ctx, tx, req)
	if err != nil {
		return resp, err
	}
	resp.LoadRequestID = loanID

	termDistribution := calculateTermDistribution(req.LoanAmount, req.LoanTerm)

	nextPaymentDate := time.Now().Add(weekDuration)
	for _, td := range termDistribution {
		paymentID, err := u.repository.InsertScheduledPayment(ctx, tx, loanID, req.CustomerID, td, nextPaymentDate)
		if err != nil {
			return resp, err
		}
		newSchedulePayment := ScheduledPayment{
			ScheduledPaymentID: paymentID,
			AmountToPay:        td,
			PaymentDate:        nextPaymentDate,
			Status:             statusPending,
		}
		resp.ScheduledPayment = append(resp.ScheduledPayment, newSchedulePayment)
		nextPaymentDate = nextPaymentDate.Add(weekDuration)
	}
	return resp, nil
}

func (u *CustomerUsecase) GetLoanList(ctx context.Context, req GetLoanListRequest) (GetLoanListResponse, error) {
	var resp GetLoanListResponse

	result, err := u.repository.GetLoanList(ctx, req.CustomerID)
	if err != nil {
		return resp, err
	}
	resp.CustomerID = req.CustomerID
	resp.Loans = result
	return resp, nil
}

func (u *CustomerUsecase) RegisterPayment(ctx context.Context, req RegisterPaymentRequest) (RegisterPaymentResponse, error) {
	var resp RegisterPaymentResponse
	var err error
	loanDetails, err := u.repository.GetLoanDetails(ctx, req.CustomerID, req.LoanID)
	if err != nil {
		return resp, err
	}

	if loanDetails.Status != statusApproved {
		return resp, ErrLoanNotApproved
	}

	paySchedule, err := u.repository.GetNextPendingPaymentSchedule(ctx, req.LoanID)
	if err != nil {
		return resp, err
	}
	resp.ScheduledPaymentID = paySchedule.ScheduledPaymentID
	resp.PaymentDate = paySchedule.PaymentDate

	if req.AmountPaid < paySchedule.AmountToPay {
		err = ErrInvalidPayment
		return resp, err
	}
	tx, err := u.repository.StartTransaction(ctx)
	if err != nil {
		return resp, err
	}
	defer func() {
		if err != nil {
			err := u.repository.Rollback(ctx, tx)
			if err != nil {
				log.Println(err)
			}
			return
		}
		err := u.repository.Commit(ctx, tx)
		if err != nil {
			log.Println(err)
		}
	}()
	if req.AmountPaid == paySchedule.AmountToPay {
		err = u.repository.ChangePaymentStatus(ctx, tx, paySchedule.ScheduledPaymentID, req.AmountPaid, statusPaid)
		if err != nil {
			return resp, err
		}
	}

	if req.AmountPaid > paySchedule.AmountToPay {
		err = u.repository.ChangePaymentStatus(ctx, tx, paySchedule.ScheduledPaymentID, req.AmountPaid, statusPaid)
		if err != nil {
			return resp, err
		}

		var (
			totalPaid uint64
			count     uint64
		)
		// Recalculate remaining term amounts
		totalPaid, count, err = u.repository.GetPaidDetails(ctx, tx, req.LoanID)
		if err != nil {
			return resp, err
		}

		if totalPaid > loanDetails.LoanAmount {
			err = fmt.Errorf("%w Total paid: %d, total loan amount: %d, excess pay: %d", ErrExcessAmount, totalPaid, loanDetails.LoanAmount, totalPaid-loanDetails.LoanAmount)
			return resp, err
		}
		remainingAmount := loanDetails.LoanAmount - totalPaid
		remainingTerm := loanDetails.LoanTerm - count

		termDistribution := calculateTermDistribution(remainingAmount, remainingTerm)

		err = u.repository.DeletePendingPayments(ctx, tx, req.LoanID)
		if err != nil {
			return resp, err
		}

		// Create new remaining payment terms
		nextPaymentDate := paySchedule.PaymentDate.Add(weekDuration)
		for _, td := range termDistribution {
			paymentID, err := u.repository.InsertScheduledPayment(ctx, tx, req.LoanID, req.CustomerID, td, nextPaymentDate)
			if err != nil {
				return resp, err
			}
			newSchedulePayment := ScheduledPayment{
				ScheduledPaymentID: paymentID,
				AmountToPay:        td,
				PaymentDate:        nextPaymentDate,
				Status:             statusPending,
			}
			resp.RemainingPayments = append(resp.RemainingPayments, newSchedulePayment)
			nextPaymentDate = nextPaymentDate.Add(weekDuration)
		}
	}

	isComplete, err := u.repository.IsLoanPaymentComplete(ctx, tx, req.LoanID)
	if err != nil {
		return resp, err
	}

	if isComplete {

		// Mark loan status to PAID
		err = u.repository.ChangeLoanStatus(ctx, tx, req.LoanID, statusPaid)
		if err != nil {
			return resp, err
		}
	}
	return resp, nil
}

// This is to make sure that we don't miss any amount during division rounding
func calculateTermDistribution(totalAmount, term uint64) []uint64 {
	if totalAmount == 0 || term == 0 {
		return nil
	}
	termAmounts := []uint64{}
	termAmount := totalAmount / term
	for term != 0 {
		totalAmount = totalAmount - termAmount
		term = term - 1
		termAmounts = append(termAmounts, termAmount)
		if term == 1 {
			termAmounts = append(termAmounts, totalAmount)
			return termAmounts
		}
	}

	return termAmounts
}
