package customer

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type handlerMocks struct {
	usecase mockICustomerUsecase
}

func newHandler(t *testing.T) (*Handler, *handlerMocks) {
	t.Helper()

	mocks := &handlerMocks{}
	return NewHandler(&mocks.usecase), mocks
}

func TestHandler_PostNewLoan(t *testing.T) {
	tests := map[string]struct {
		setMocks       func(*handlerMocks)
		setBody        []byte
		expectedStatus int
	}{
		"success: create new loan": {
			setMocks: func(hm *handlerMocks) {
				hm.usecase.Mock.On("PostNewLoan", mock.Anything, PostNewLoanRequest{
					CustomerID: 1,
					LoanAmount: 10000,
					LoanTerm:   5,
				}).Return(PostNewLoanResponse{}, nil)
			},
			setBody:        []byte(`{"loan_amount": 10000, "loan_term": 5}`),
			expectedStatus: http.StatusOK,
		},
		"fail: When loan amount is zero": {
			setMocks: func(hm *handlerMocks) {
				hm.usecase.Mock.On("PostNewLoan", mock.Anything, PostNewLoanRequest{
					CustomerID: 1,
					LoanAmount: 0,
					LoanTerm:   5,
				}).Return(PostNewLoanResponse{}, nil)
			},
			setBody:        []byte(`{"loan_amount": 0, "loan_term": 5}`),
			expectedStatus: http.StatusBadRequest,
		},
		"fail: When loan term is zero": {
			setMocks: func(hm *handlerMocks) {
				hm.usecase.Mock.On("PostNewLoan", mock.Anything, PostNewLoanRequest{
					CustomerID: 1,
					LoanAmount: 10000,
					LoanTerm:   0,
				}).Return(PostNewLoanResponse{}, nil)
			},
			setBody:        []byte(`{"loan_amount": 10000, "loan_term": 0}`),
			expectedStatus: http.StatusBadRequest,
		},
		"fail: When loan term more than loan amount": {
			setMocks: func(hm *handlerMocks) {
				hm.usecase.Mock.On("PostNewLoan", mock.Anything, PostNewLoanRequest{
					CustomerID: 1,
					LoanAmount: 10000,
					LoanTerm:   20000,
				}).Return(PostNewLoanResponse{}, nil)
			},
			setBody:        []byte(`{"loan_amount": 10000, "loan_term": 20000}`),
			expectedStatus: http.StatusBadRequest,
		},
		"fail: When loan amount in req is negative": {
			setMocks: func(hm *handlerMocks) {
			},
			setBody:        []byte(`{"loan_amount": -10000, "loan_term": 5}`),
			expectedStatus: http.StatusBadRequest,
		},
		"fail: When usecase fails, return 500": {
			setMocks: func(hm *handlerMocks) {
				hm.usecase.Mock.On("PostNewLoan", mock.Anything, mock.Anything).Return(PostNewLoanResponse{}, fmt.Errorf("Error in usecase: something went wrong"))
			},
			setBody:        []byte(`{"loan_amount": 10000, "loan_term": 5}`),
			expectedStatus: http.StatusInternalServerError,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			h, hm := newHandler(t)
			tt.setMocks(hm)

			rec := httptest.NewRecorder()
			mux := chi.NewRouter()
			mux.Use(jwtauth.Verifier(tokenAuth))
			mux.Use(jwtauth.Authenticator)
			mux.HandleFunc("/v1/customer/loan", h.PostNewLoan)
			req, err := http.NewRequest("POST", "/v1/customer/loan", bytes.NewReader(tt.setBody))
			if err != nil {
				panic(err)
			}
			req.Header.Add("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcl9pZCI6MX0.MHJlCibhF93jN73yX_5JReYqPEs2rN8Ird9CiUpIL-0")
			mux.ServeHTTP(rec, req)
			assert.Equal(t, tt.expectedStatus, rec.Code)
			fmt.Println(rec.Body)
		})
	}
}

func TestHandler_GetLoanList(t *testing.T) {
	tests := map[string]struct {
		setMocks       func(*handlerMocks)
		expectedStatus int
	}{
		"success: Get valid loan list": {
			setMocks: func(hm *handlerMocks) {
				hm.usecase.Mock.On("GetLoanList", mock.Anything, GetLoanListRequest{
					CustomerID: 1,
				}).Return(GetLoanListResponse{}, nil)
			},
			expectedStatus: http.StatusOK,
		},
		"fail: When usecase fails, return 500": {
			setMocks: func(hm *handlerMocks) {
				hm.usecase.Mock.On("GetLoanList", mock.Anything, GetLoanListRequest{
					CustomerID: 1,
				}).Return(GetLoanListResponse{}, fmt.Errorf("Some error"))
			},
			expectedStatus: http.StatusInternalServerError,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			h, hm := newHandler(t)
			tt.setMocks(hm)

			rec := httptest.NewRecorder()
			mux := chi.NewRouter()
			mux.Use(jwtauth.Verifier(tokenAuth))
			mux.Use(jwtauth.Authenticator)
			mux.HandleFunc("/v1/customer/loan", h.GetLoanList)
			req, err := http.NewRequest("GET", "/v1/customer/loan", nil)
			if err != nil {
				panic(err)
			}
			req.Header.Add("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcl9pZCI6MX0.MHJlCibhF93jN73yX_5JReYqPEs2rN8Ird9CiUpIL-0")
			mux.ServeHTTP(rec, req)
			assert.Equal(t, tt.expectedStatus, rec.Code)
			fmt.Println(rec.Body)
		})
	}
}
