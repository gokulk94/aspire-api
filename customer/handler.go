package customer

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	"gitlab.com/gokulk94/aspire-api/utils"
)

// Handler struct ...
type Handler struct {
	usecase iCustomerUsecase
}

// NewHandler ...
func NewHandler(cu iCustomerUsecase) *Handler {
	return &Handler{cu}
}

// Route initializes and defines API routes for customer endpoint.
// Also initializes middlewares. jwtauth middleware used is for demonstration purpose only.
func Route(h *Handler) chi.Router {
	r := chi.NewRouter()

	r.Use(jwtauth.Verifier(tokenAuth))
	r.Use(jwtauth.Authenticator)

	r.Post("/loan", h.PostNewLoan)

	r.Get("/loan", h.GetLoanList)

	r.Post("/loan/payment", h.RegisterPayment)
	return r
}

// PostNewLoan creates a new loan request.
// Requires a customer_id from auth, loan_amount and loan_term in req body
func (h *Handler) PostNewLoan(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	req := PostNewLoanRequest{}
	_, claims, _ := jwtauth.FromContext(ctx)

	reqCustomerID, ok := claims["customer_id"].(float64)
	if !ok {
		utils.Fail(w, http.StatusBadRequest, "Bad customer_id")
		return
	}

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		log.Println("Failed to parse request body", err)
		utils.Fail(w, http.StatusBadRequest, "failed to decode request body")
		return
	}
	req.CustomerID = uint64(reqCustomerID)

	// Validation
	if req.LoanAmount == 0 || req.LoanTerm == 0 {
		utils.Fail(w, http.StatusBadRequest, "loan_amount or loan_term should not be zero")
		return
	}

	if req.LoanAmount < req.LoanTerm {
		utils.Fail(w, http.StatusBadRequest, "loan term should not be more than loan amount")
		return
	}
	resp, err := h.usecase.PostNewLoan(ctx, req)
	if err != nil {
		log.Println("Error in PostNewLoan", err)
		utils.Fail(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}

	utils.Send(w, http.StatusOK, resp)
}

// GetLoanList returns list of customer loans including repayment schedules
// customer_id is fetched from auth, so given customer_id can see their own loans only.
func (h *Handler) GetLoanList(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	req := GetLoanListRequest{}

	_, claims, _ := jwtauth.FromContext(ctx)

	reqCustomerID, ok := claims["customer_id"].(float64)
	if !ok {
		utils.Fail(w, http.StatusBadRequest, "Bad customer_id")
		return
	}

	req.CustomerID = uint64(reqCustomerID)
	resp, err := h.usecase.GetLoanList(ctx, req)
	if err != nil {
		log.Println(err)
		utils.Fail(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}
	utils.Send(w, http.StatusOK, resp)
}

// RegisterPayment registers a new payment from customer for a given loan_id
// Rules:
// - One customer cannot pay another customers loan
// - Payment would be processed against closest pending payment date.
// - Amount paid should be >= scheduled loan payment amount
// - If amount paid is greater than scheduled loan payment amount
// 		- API will accept the incoming payment.
// 		- API will remove future payments schedules and recreate new payment schedules based on remaining loan amount and loan term
// - If all pending payments are done, Loan status will be changed to PAID
func (h *Handler) RegisterPayment(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	req := RegisterPaymentRequest{}
	_, claims, _ := jwtauth.FromContext(ctx)

	reqCustomerID, ok := claims["customer_id"].(float64)
	if !ok {
		utils.Fail(w, http.StatusBadRequest, "Bad customer_id")
		return
	}

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		log.Println("Failed to parse request body", err)
		utils.Fail(w, http.StatusBadRequest, "failed to decode request body")
		return
	}
	req.CustomerID = uint64(reqCustomerID)
	if req.AmountPaid == 0 {
		utils.Fail(w, http.StatusBadRequest, "amount paid is zero")
		return
	}
	resp, err := h.usecase.RegisterPayment(ctx, req)
	if err != nil {
		log.Println(err)
		if errors.Is(err, ErrLoanNotApproved) {
			utils.Fail(w, http.StatusBadRequest, err.Error())
			return
		}
		if errors.Is(err, ErrInvalidPayment) {
			utils.Fail(w, http.StatusBadRequest, err.Error())
			return
		}
		if errors.Is(err, ErrExcessAmount) {
			utils.Fail(w, http.StatusBadRequest, err.Error())
			return
		}
		utils.Fail(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}

	utils.Send(w, http.StatusOK, resp)
}
