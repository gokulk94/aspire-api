CREATE TABLE public.customer (
  id_customer serial PRIMARY KEY,
  name text NOT NULL,
  age int NOT NULL,
  address text NOT NULL,
  created_at timestamp,
  updated_at timestamp
);

CREATE TABLE public.loan_request (
  id_loan_request serial PRIMARY KEY,
  id_customer int4 references customer(id_customer),
  loan_amount bigint, -- in smallest currency unit. For dollar, it's cent.
  loan_term int,
  loan_frequency int DEFAULT 7, -- in number of days, defult 7
  status text,
  created_at timestamp,
  updated_at timestamp
);

CREATE TABLE public.scheduled_payment (
  id_scheduled_payment serial PRIMARY KEY,
  id_customer int4 references customer(id_customer),
  id_loan_request int4 references loan_request(id_loan_request),
  amount_to_pay bigint,
  amount_paid bigint default 0,
  payment_date timestamp,
  status text,
  created_at timestamp,
  updated_at timestamp
);

