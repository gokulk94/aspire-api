## About The Project - Aspire Backend Code Challenge

_Note: This project is built based on the requirements sent in the Backend Code challenge assignment._

Aspire API is a backend API services for a simplified loan management service.

The service provides a couple of APIs for customers to create and operate loans and some APIs for admin to manage loan status.

### Built With

* Golang
* Postgres
* go-chi
* docker, docker-compose


<!-- GETTING STARTED -->
## Getting Started

(TBA)

### Prerequisites

Requires golang and docker installed to run and build the project.

_Ubuntu installation guide listed below_
* golang
  ```sh
   sudo add-apt-repository ppa:longsleep/golang-backports
   sudo apt update
   sudo apt install golang-go
  ```
* docker, docker-compose
  ```sh
   # install docker
   sudo apt install apt-transport-https ca-certificates curl software-properties-common
   curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
   sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
   sudo apt update
   sudo apt install docker-ce
   sudo usermod -aG docker ${USER}
   su - ${USER}


   # install docker-compose.
   # Check for new version at https://github.com/docker/compose/releases
   sudo curl -L "https://github.com/docker/compose/releases/download/v2.5.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

   sudo chmod +x /usr/local/bin/docker-compose
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/gokulk94/aspire-api.git
   ```
2. Build and run the application using docker-compose
   ```sh
   docker-compose up --build
   ```

Note: "docker-compose up --build" will build application, setup DB, run DB migrations, run application all in one command.


## Usage

1. Use postman collection available in `doc/` folder to call the APIs.
2. API server will log some sample JWT tokens that you can use to call the API.
   ```
   aspire-api      | DEBUG: a sample admin jwt is eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbl9pZCI6MX0.4TEetM7B6uIWWsH8_r0auF_8YidA7pa6fuERWMwHat0
   aspire-api      | 
   aspire-api      | DEBUG: a sample customer jwt is eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcl9pZCI6MX0.MHJlCibhF93jN73yX_5JReYqPEs2rN8Ird9CiUpIL-0
   ```
3. DB migration will create a sample customer with customer_id = 1

## Unit test

Status: WIP

To run Unit tests
```
make create-mocks
make run-test
```
Note: Unit tests were added at the end, so it may be incomplete.

## Decisions made by me for the project

- All Time values are in UTC
- Code/Module separation is based on Clean code architecture.
  - Clean code arch is easier to read and understand even if the project grows to a larger code base.
  - Flow: Router -> Handler -> Usecase -> Repository
  - I also have worked with Clean Code architecture before, but modified based on project requirement.
- go-chi is lightweight and not too imposing.
  - I find simpler dependencies are easier to manage. That's why I didn't go with full fledged frameworks like fiber.
- Database: 
  - Considering this is a banking related project, RDBMS like postgres is the better choice.
  - I also decided to not use ORM and go with standard database/sql and raw queries.
  - For this projects usecase, raw queries feels better choice.
  - For complex applications we can use some in-house query builder or external query builder packages
- Amount type
  - Currency is assumed to be USD.
  - All amounts are in cents. 100 cents = $1
  - All API requests assumes amounts to be in the smallest unit of the regional currency. In this projects case, it's cents


## Task-List

- [x] Build fully functional REST API without any UI
- [x] README.md should contain all the information that the reviewers need to run and use the app
- [x] Write code with your teammates in mind: readable, easy to review & understand
- [ ] Unit and Features tests are always welcome
- [ ] The quality of the tests is one of the key factors
- [x] Include brief documentation for the project: the choices you made and why


