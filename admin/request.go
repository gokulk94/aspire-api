package admin

type LoanApproveStatusRequest struct {
	LoanID    uint64 `json:"loan_id"`
	NewStatus string `json:"-"`
}
