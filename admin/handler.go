package admin

import (
	"errors"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	"gitlab.com/gokulk94/aspire-api/utils"
)

type Handler struct {
	usecase iAdminUsecase
}

func NewHandler(au iAdminUsecase) *Handler {
	return &Handler{au}
}

func Route(h *Handler) chi.Router {
	r := chi.NewRouter()

	r.Use(jwtauth.Verifier(tokenAuth))
	r.Use(jwtauth.Authenticator)

	r.Get("/loan", h.GetLoanList)

	r.Post("/loan/{loan_id}/approve", h.ApproveLoan)

	r.Post("/loan/{loan_id}/reject", h.RejectLoan)
	return r
}

// GetLoanList returns all loan requests and its payment details
func (h *Handler) GetLoanList(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	_, claims, _ := jwtauth.FromContext(ctx)

	reqAdminID, ok := claims["admin_id"].(float64)
	if !ok {
		utils.Fail(w, http.StatusBadRequest, "Bad admin_id")
		return
	}
	adminID := uint64(reqAdminID)
	log.Println(adminID)

	resp, err := h.usecase.GetLoanList(ctx)
	if err != nil {
		log.Println(err)
		utils.Fail(w, http.StatusBadRequest, "Internal Server Error")
		return
	}
	utils.Send(w, http.StatusOK, resp)
}

// ApproveLoan approves a given loan.
// It will change status PENDING -> APPROVED.
// If as-is status is not PENDING, no change to status so that already approved/rejected or fully paid loan status are not changed.
func (h *Handler) ApproveLoan(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var req LoanApproveStatusRequest
	var err error

	_, claims, _ := jwtauth.FromContext(ctx)

	reqAdminID, ok := claims["admin_id"].(float64)
	if !ok {
		utils.Fail(w, http.StatusBadRequest, "Bad admin_id")
		return
	}
	adminID := uint64(reqAdminID)
	log.Println(adminID)
	req.LoanID, err = strconv.ParseUint(chi.URLParam(r, "loan_id"), 10, 64)
	if req.LoanID == 0 || err != nil {
		utils.Fail(w, http.StatusBadRequest, "Internal Server Error")
		return
	}

	req.NewStatus = "APPROVED"
	err = h.usecase.ChangeLoanStatus(ctx, req)
	if err != nil {
		log.Println(err)
		if errors.Is(err, ErrStatusNotUpdated) {
			utils.Fail(w, http.StatusBadRequest, "Previous loan status is not \"PENDING\" status, so status not updated")
			return
		}
		utils.Fail(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}
	utils.Send(w, http.StatusOK, "APPROVED")
}

// ApproveLoan approves a given loan.
// It will change status PENDING -> REJECTED.
// If as-is status is not PENDING, no change to status so that already approved/rejected or fully paid loan status are not changed.
func (h *Handler) RejectLoan(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var req LoanApproveStatusRequest
	var err error

	_, claims, _ := jwtauth.FromContext(ctx)

	reqAdminID, ok := claims["admin_id"].(float64)
	if !ok {
		utils.Fail(w, http.StatusBadRequest, "Bad admin_id")
		return
	}
	adminID := uint64(reqAdminID)
	log.Println(adminID)

	req.LoanID, err = strconv.ParseUint(chi.URLParam(r, "loan_id"), 10, 64)
	if req.LoanID == 0 || err != nil {
		utils.Fail(w, http.StatusBadRequest, "Internal Server Error")
		return
	}
	if req.LoanID == 0 {
		utils.Fail(w, http.StatusBadRequest, "Internal Server Error")
		return
	}

	req.NewStatus = "REJECTED"
	err = h.usecase.ChangeLoanStatus(ctx, req)
	if err != nil {
		log.Println(err)
		if errors.Is(err, ErrStatusNotUpdated) {
			utils.Fail(w, http.StatusBadRequest, "Previous loan status is not PENDING, so status not updated")
			return
		}
		utils.Fail(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}
	utils.Send(w, http.StatusOK, "REJECTED")
}
