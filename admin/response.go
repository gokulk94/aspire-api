package admin

type GetLoanListResponse struct {
	Status int    `json:"status"`
	Loans  []Loan `json:"loans"`
}

type Loan struct {
	LoadRequestID     uint64 `json:"id_loan_request"`
	LoanAmount        uint64 `json:"loan_amount"`
	LoanTerm          uint64 `json:"loan_term"`
	Status            string `json:"status"`
	ScheduledPayments []ScheduledPayment
}

type ScheduledPayment struct {
	ScheduledPaymentID uint64 `json:"id_scheduled_payment"`
	AmountToPay        uint64 `json:"amount_to_pay"`
	PaymentDate        string `json:"payment_date"`
	Status             string `json:"status"`
}
