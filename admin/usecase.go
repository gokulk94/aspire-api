package admin

import (
	"context"
	"net/http"
)

type iAdminUsecase interface {
	GetLoanList(ctx context.Context) (GetLoanListResponse, error)
	ChangeLoanStatus(ctx context.Context, req LoanApproveStatusRequest) error
}

type AdminUsecase struct {
	repository iAdminRepo
}

func NewUsecase(r iAdminRepo) *AdminUsecase {

	return &AdminUsecase{r}
}

func (u *AdminUsecase) GetLoanList(ctx context.Context) (GetLoanListResponse, error) {
	var resp GetLoanListResponse

	result, err := u.repository.GetLoanList(ctx)
	if err != nil {
		resp.Status = http.StatusInternalServerError
		return resp, err
	}
	resp.Loans = result
	resp.Status = http.StatusOK
	return resp, nil
}

func (u *AdminUsecase) ChangeLoanStatus(ctx context.Context, req LoanApproveStatusRequest) error {

	err := u.repository.ChangeLoanStatus(ctx, req)

	return err

}
