package admin

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
)

var (
	ErrStatusNotUpdated = fmt.Errorf("Status not updated")
)

type iAdminRepo interface {
	GetLoanList(ctx context.Context) ([]Loan, error)
	ChangeLoanStatus(ctx context.Context, req LoanApproveStatusRequest) error
}

type AdminRepo struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) *AdminRepo {
	return &AdminRepo{db}
}

func (r *AdminRepo) GetLoanList(ctx context.Context) ([]Loan, error) {
	loans := []Loan{}
	query := `
	select
	lr.id_loan_request,
	lr.loan_amount,
	lr.loan_term,
	lr.status,
	json_agg(json_build_object('id_scheduled_payment', sp.id_scheduled_payment, 'amount_to_pay', sp.amount_to_pay, 'payment_date', sp.payment_date, 'status', sp.status) order by sp.payment_date asc)
from
	loan_request lr
join scheduled_payment sp on
	sp.id_loan_request = lr.id_loan_request
group by lr.id_loan_request
order by lr.created_at asc
	`
	rows, err := r.db.QueryContext(ctx, query)
	if err != nil {
		return loans, err
	}
	defer rows.Close()

	for rows.Next() {
		var l Loan
		var pdStr string
		err := rows.Scan(&l.LoadRequestID, &l.LoanAmount, &l.LoanTerm, &l.Status, &pdStr)
		if err != nil {
			return loans, err
		}
		var sp []ScheduledPayment
		err = json.Unmarshal([]byte(pdStr), &sp)
		if err != nil {
			return loans, err
		}
		l.ScheduledPayments = sp
		loans = append(loans, l)
	}
	return loans, nil
}

func (r *AdminRepo) ChangeLoanStatus(ctx context.Context, req LoanApproveStatusRequest) error {
	rowsAffected, err := r.db.ExecContext(ctx, `UPDATE loan_request SET status = $1 where id_loan_request = $2 AND status = 'PENDING'`, req.NewStatus, req.LoanID)
	if err != nil {
		return err
	}

	if ra, err := rowsAffected.RowsAffected(); ra == 0 || err != nil {
		return ErrStatusNotUpdated
	} else {
		log.Println(ra, req)
	}
	return nil
}
