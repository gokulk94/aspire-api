package router

import (
	"database/sql"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/gokulk94/aspire-api/admin"
	"gitlab.com/gokulk94/aspire-api/customer"
	"gitlab.com/gokulk94/aspire-api/utils"
)

func NewRouter(db *sql.DB) chi.Router {
	r := chi.NewRouter()

	// Some standard middlewares for tracking and recovery
	r.Use(middleware.RealIP)
	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Use(middleware.Timeout(60 * time.Second))

	// For healthcheck to be used for ALB
	r.Get("/status", healthChecker())

	r.Route("/v1", func(r chi.Router) {
		ar := admin.NewRepository(db)
		au := admin.NewUsecase(ar)
		ah := admin.NewHandler(au)
		r.Mount("/admin", admin.Route(ah))

		cr := customer.NewRepository(db)
		cu := customer.NewUsecase(cr)
		ch := customer.NewHandler(cu)
		r.Mount("/customer", customer.Route(ch))
	})
	return r
}

// Simple healthchecker to validate that API is up and running. Usually used by Load balancers/ASG
// Additionally we can also add commit id in response to track API version, but not implemented for now.
func healthChecker() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		utils.Send(w, http.StatusOK, "API is up and running...")
	}
}
