package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/gokulk94/aspire-api/router"
	"gitlab.com/gokulk94/aspire-api/utils"
)

func init() {
	viper.SetConfigFile("./config/config.json")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func main() {

	// init database
	db := utils.MustInitDatabase()
	defer db.Close()

	server := http.Server{
		Addr:    viper.GetString("server.address"),
		Handler: router.NewRouter(db),
	}
	server.SetKeepAlivesEnabled(false)

	go func() {
		log.Printf("Starting server at %s", server.Addr)
		err := server.ListenAndServe()
		if err != http.ErrServerClosed {
			log.Panic("failed to start server: ", err)
		}
	}()

	// support graceful shut down
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)
	stopSignal := <-stop
	log.Printf("received %s signal", stopSignal.String())
	shutDownTimeout := 5 // in seconds
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(shutDownTimeout)*time.Second)
	defer func() {
		cancel()
	}()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}
	log.Println("Server Exited Properly")
}
