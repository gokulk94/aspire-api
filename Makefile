api-build:
	docker-compose build

api-run:
	docker_compose up --build

create-mocks:
	go install github.com/vektra/mockery/v2@latest
	mockery --dir ./customer/ --inpackage --all

run-test:
	go test ./customer/ --cover