package utils

import (
	"encoding/json"
	"net/http"
)

type APIFailResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type APISuccessResponse struct {
	Status string          `json:"status"`
	Result json.RawMessage `json:"result,omitempty"`
}

func Send(w http.ResponseWriter, status int, result interface{}) {
	rjson, err := json.Marshal(result)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	r := APISuccessResponse{
		Status: http.StatusText(status),
		Result: rjson,
	}

	j, err := json.Marshal(r)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(j)
}

func Fail(w http.ResponseWriter, status int, message string) {
	r := APIFailResponse{
		Status:  http.StatusText(status),
		Message: message,
	}
	rjson, err := json.Marshal(r)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(rjson)
}
