package utils

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/lib/pq"
	"github.com/spf13/viper"
)

const timeLayout = "2006-01-02T15:04:05"

func MustInitDatabase() *sql.DB {
	var (
		dbHost = viper.GetString(`database.host`)
		dbPort = viper.GetString(`database.port`)
		dbUser = viper.GetString(`database.user`)
		dbPass = viper.GetString(`database.pass`)
		dbName = viper.GetString(`database.name`)
	)

	connString := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable&search_path=public", dbUser, dbPass, dbHost, dbPort, dbName)
	db, err := sql.Open("postgres", connString)
	if err != nil {
		panic(err)
	}
	err = db.Ping()
	if err != nil {
		panic(err)
	}

	db.SetConnMaxLifetime(time.Duration(viper.GetInt(`database.setting.conn_max_lifetime`)) * time.Minute)
	db.SetMaxIdleConns(viper.GetInt(`database.setting.max_idle_conn`))
	db.SetMaxOpenConns(viper.GetInt(`database.setting.max_open_conn`))
	return db
}

func ParseTime(str string) (time.Time, error) {
	var (
		t   time.Time
		err error
	)
	if str != "" {
		t, err = time.Parse(timeLayout, str)
		if err != nil {
			return t, err
		}
	}
	return t, err
}
